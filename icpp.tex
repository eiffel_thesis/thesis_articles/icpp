\documentclass[sigconf]{acmart}
\usepackage[binary-units=true]{siunitx}
\usepackage{subcaption}
% defining the \BibTeX command - from Oren Patashnik's original BibTeX documentation.
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08emT\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}

% Rights management information.
% This information is sent to you when you complete the rights form.
% These commands have SAMPLE values in them; it is your responsibility as an author to replace
% the commands and values with those provided to you when you complete the rights form.
%
% These commands are for a PROCEEDINGS abstract or paper.
\copyrightyear{2019}
\acmYear{2019}
\setcopyright{acmlicensed}
\acmConference[Woodstock '18]{Woodstock '18: ACM Symposium on Neural Gaze Detection}{June 03--05, 2018}{Woodstock, NY}
\acmBooktitle{Woodstock '18: ACM Symposium on Neural Gaze Detection, June 03--05, 2018, Woodstock, NY}
\acmPrice{15.00}
\acmDOI{10.1145/1122445.1122456}
\acmISBN{978-1-4503-9999-9/18/06}

\begin{document}

% The "title" command has an optional parameter, allowing the author to define a "short title" to be used in page headers.
\title{Title}

% The "author" command and its associated commands are used to define the authors and their affiliations.
% Of note is the shared affiliation of the first two authors, and the "authornote" and "authornotemark" commands
% used to denote shared contribution to the research.
\author{Ben Trovato}
\authornote{Both authors contributed equally to this research.}
\email{trovato@corporation.com}
\orcid{1234-5678-9012}
\author{G.K.M. Tobin}
\authornotemark[1]
\email{webmaster@marysville-ohio.com}
\affiliation{%
  \institution{Institute for Clarity in Documentation}
  \streetaddress{P.O. Box 1212}
  \city{Dublin}
  \state{Ohio}
  \postcode{43017-6221}
}

\author{Lars Thrvld}
\affiliation{%
  \institution{The Th{\o}rv{\"a}ld Group}
  \streetaddress{1 Th{\o}rv{\"a}ld Circle}
  \city{Hekla}
  \country{Iceland}}
\email{larst@affiliation.org}

\author{Valerie Beranger}
\affiliation{%
  \institution{Inria Paris-Rocquencourt}
  \city{Rocquencourt}
  \country{France}
}

% By default, the full list of authors will be used in the page headers. Often, this list is too long, and will overlap
% other information printed in the page headers. This command allows the author to define a more concise list
% of authors' names for this purpose.
\renewcommand{\shortauthors}{Trovato and Tobin, et al.}


\begin{abstract}
% What is the problem?
Handling of memory pressure in container-based cloud is not fair between containers as it does not take into account their performance states.
% Why is it important?
During those events, containers' performances can become bad so they will not meet their Quality of Service (QoS).
% What is your idea?
We propose to make a better use of the available memory based on performance states of running containers through memory consolidation to increase their performances.
% How is it better?
\end{abstract}

% The code below is generated by the tool at http://dl.acm.org/ccs.cfm.
% Please copy and paste the code instead of the example below.
\begin{CCSXML}
<ccs2012>
 <concept>
  <concept_id>10010520.10010553.10010562</concept_id>
  <concept_desc>Computer systems organization~Embedded systems</concept_desc>
  <concept_significance>500</concept_significance>
 </concept>
 <concept>
  <concept_id>10010520.10010575.10010755</concept_id>
  <concept_desc>Computer systems organization~Redundancy</concept_desc>
  <concept_significance>300</concept_significance>
 </concept>
 <concept>
  <concept_id>10010520.10010553.10010554</concept_id>
  <concept_desc>Computer systems organization~Robotics</concept_desc>
  <concept_significance>100</concept_significance>
 </concept>
 <concept>
  <concept_id>10003033.10003083.10003095</concept_id>
  <concept_desc>Networks~Network reliability</concept_desc>
  <concept_significance>100</concept_significance>
 </concept>
</ccs2012>
\end{CCSXML}

\ccsdesc[500]{Computer systems organization~Embedded systems}
\ccsdesc[300]{Computer systems organization~Redundancy}
\ccsdesc{Computer systems organization~Robotics}
\ccsdesc[100]{Networks~Network reliability}

% Keywords. The author(s) should pick words that accurately describe the work being
% presented. Separate the keywords with commas.
\keywords{datasets, neural networks, gaze detection, text tagging}

\maketitle

\section{Introduction}
\label{sec:intro}
The way how memory pressure is handled in cloud-based containers is not fair between containers.

Nowadays, cloud platforms which make use of container are a reality \cite{amazon_amazon_nodate-1, microsoft_microsoft_nodate-1, alibaba_alibaba_nodate-1, ovh_ovh_nodate-1}.
Container \cite{docker_docker_nodate, lxc_lxc_nodate} is a Linux technology which uses Linux kernel features such as namespaces and control groups \cite{rami_rosen_namespace_2016, zhenyun_zhuang_taming_2017} to virtualize applications and restraint their executions.
They permit virtualization and present the advantages to be lighter, there is no guest Operating System (OS), and faster than Virtual Machines (VM), boot time of \SI{\approx 1}{\second} compared to \SI{\approx 10}{\second}.

During memory pressure the kernel will reclaim memory of running containers.
It can be a problem for their performances and resource usage.
Indeed, there is two problematic cases:
\begin{itemize}
	\item Either the kernel reclaimed less memory than the container's Working Set Size (WSS) \cite{denning_working_1967} so its performances will still be good but the kernel could have reclaimed more memory so resource usage is not optimal.
	\item Or the kernel reclaimed too much memory and the container's memory is below its WSS so its performances are bad.
\end{itemize}

Our proposal is the following.
We make the assumption that the kernel has knowledge of the containers' performance states thanks to applicative probes.
Based on this information the kernel will have a better idea of how memory can be reclaimed from a container and will firstly take memory from well-performing ones.
This permits to be fairer with bad-performing containers and give them a chance to grab more memory.

Our article is composed of the following parts: in section \ref{sec:background} we will describe more precisely the mechanism we talked about and why it can be a problem. In section \ref{sec:solution} we focus on how our solution works. Section \ref{sec:settings} and \ref{sec:results} give the experimental settings and results. In section \ref{sec:related_works} we discuss other works related to our.

\section{Technical background}
\label{sec:background}
Containers stand on control groups \cite{hiroyu_cgroup_2008}.
As their names say those groups permits to finely control applications' resources by subscribing processus in them.
For example, it is possible to restraint on which CPU cores the processus will run or limit the amount of memory a processus can use.
The second example is handled by the memory control group \cite{zhenyun_zhuang_taming_2017}.
One specificity of this control group will be explained in the next section and the problem it generates will be detailed after.

\subsection{\texttt{soft limit} mechanism}
Memory control group have multiples limits and one of them is the \texttt{soft limit} \cite{linux_memory_nodate}.
As explained in \ref{sec:intro}, the kernel will push containers which exceed their \texttt{soft limits} to those values during memory pressure.
No guarantee comes with \texttt{soft limit} so it is possible that containers see their memory pushed below.
The goal of this mechanism is to ensure a certain amount of memory to containers even during memory pressure.
Ideally this limit should be set to the WSS of the container but finding this value at run-time is hard \cite{gregg_working_2018, nitu_working_2018}.
Even if this mechanism seems caring the difficulty to properly set the \texttt{soft limit} can make it evil.

\subsection{The \textit{wasted case}}
Imagine that two applications A and B whom WSS are respectively \SI{2}{\giga\byte} and \SI{1}{\giga\byte} run in container with \texttt{soft limit} fixed to \SI{2.5}{\giga\byte} and \SI{.5}{\giga\byte} on computer with \SI{3}{\giga\byte} of memory.
Since the sum of the WSS is equal to the computer total memory there will be memory pressure so the mechanism will be activated and B will be pushed to its \texttt{soft limit} but not A because it does not exceed its limit.
A's performances will be good because its working set is in memory while B's will be really bad.
So A wastes \SI{.5}{\giga\byte} which can be better used if B's \texttt{soft limit} was set half a gigabyte higher.
This case is presented in figure .

\subsection{The \textit{over-set case}}
Another problematic case is when sum of \texttt{soft limits} is greater than total memory available on the computer.
The kernel will not be able to push containers to their \texttt{soft limits} and even if those limits were set to the WSS the containers' performances will be bad.
In this case it would be better to take memory of a container to give it to another to try to set the other container's memory near its WSS.
% IDEA: force_empty a random red container to redistribute memory
Figure shows this case.

\section{}
\label{sec:solution}
Algorithm's pseudo code.

\section{Experimental settings}
\label{sec:settings}

\section{Results}
\label{sec:results}

\section{Related works}
\label{sec:related_works}

\section{Conclusion}

%
% The acknowledgments section is defined using the "acks" environment (and NOT an unnumbered section). This ensures
% the proper identification of the section in the article metadata, and the consistent spelling of the heading.
\begin{acks}
To Robert, for the bagels and explaining CMYK and color spaces.
\end{acks}

\bibliographystyle{ACM-Reference-Format}
\bibliography{icpp}

\end{document}